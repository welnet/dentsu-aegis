import { DuplicateComponent } from './components/duplicate/duplicate.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomeComponent} from './components/home/home.component';
import { SampleTemplateComponent } from './components/sample-template/sample-template.component';
import { GetDataComponent } from './components/get-data/get-data.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'sample-template', component: SampleTemplateComponent },
  { path: 'duplicate', component: DuplicateComponent },
  { path: 'get-data', component: GetDataComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {}
