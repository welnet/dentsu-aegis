import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable()
export class DataService {
  private apiUrl = 'https://baconipsum.com/api/?type=all-meat&paras=3&start-with-lorem=1&format=json';
  private apiUrl2 = 'https://baconipsum.com/api/?type=meat-and-filler&paras=5&format=json';

  constructor(private http: Http) {}

  getData() {
    return this.http.get(this.apiUrl).pipe(map((response: Response) => response.json()));
  }

  getData2() {
    return this.http.get(this.apiUrl2).pipe(map((response: Response) => response.json()));
  }

}
