import { DataService } from '../../services/data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-get-data',
  templateUrl: './get-data.component.html',
  styleUrls: ['./get-data.component.scss']
})
export class GetDataComponent implements OnInit {

  data: any = [];
  data2: any = [];

  dataFromFirstApi = this.data;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.getData();
    this.getData2();
  }

  getData(): void {
    this.dataService.getData().subscribe(data => {
      this.data = data;
    });
  }

  getData2(): void {
    this.dataService.getData2().subscribe(data2 => this.data2 = data2);
  }

  stringifyData() {
    const stringifyData = this.getData();
    console.log(stringifyData);
  }
}
