import { Component } from '@angular/core';

@Component({
  selector: 'app-duplicate',
  templateUrl: './duplicate.component.html',
  styleUrls: ['./duplicate.component.scss']
})
export class DuplicateComponent {

  numbers = [1, 2, 3, 4];
  numbers2 = [1, 2, 3, 4, 5, 6, 7, 8, 9];

  duplicateArray() {
    const duplicateArray = this.numbers.concat(this.numbers);
    return duplicateArray;
  }

  removeEvenElements() {
    const oddElements = this.numbers2.filter(a => this.numbers2.indexOf(a) % 2 === 0);
    return oddElements;
  }

}
